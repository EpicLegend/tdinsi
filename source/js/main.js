'use strict';

//= ../node_modules/jquery/dist/jquery.js
//= ../node_modules/bootstrap/dist/js/bootstrap.js
//= ../node_modules/lazysizes/lazysizes.js
//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js

//= components/sliders.js
//= library/wow.js
//= library/jquery-ui.js
//= library/slick.js




$(document).ready(function () {


	// START MENU
	var body = document.querySelector("#page");
	var burger = document.querySelector("#burger");
	var modalMenu = document.querySelector(".menu-modal");
	var modalMenuContent = modalMenu.querySelector(".menu-modal__content");
	var modalMenuClose = modalMenu.querySelector(".menu-modal__close");
	var minicart = document.querySelector(".minicart__link_btn");

	burger.addEventListener("click", function (e) {
		modalMenu.classList.add("menu-modal_active");
		boduHidden();
	});
	modalMenuClose.addEventListener("click", function (e) {
		modalMenu.classList.remove("menu-modal_active");
		bodyDefault();
	});
	modalMenu.addEventListener("click", function (e) {
		modalMenu.classList.remove("menu-modal_active");
		bodyDefault();
	});
	modalMenuContent.addEventListener("click", function (e) {
		e.stopPropagation();
	});
	// END MENU


	// START minicart
	var minicart = document.querySelector(".minicart__link_btn");
	var modalMininCart = document.querySelector(".minicart__dropdown");
	var modalMininCartContent = modalMininCart.querySelector(".minicart__dropdown_content");
	var modalCloseMiniCart = modalMininCart.querySelector(".minicart__dropdown_close");
	
	minicart.addEventListener("click", function (e) {
		modalMininCart.classList.add("minicart__dropdown_active");
		boduHidden();
	});
	modalCloseMiniCart.addEventListener("click", function (e) {
		modalMininCart.classList.remove("minicart__dropdown_active");
		bodyDefault();
	});
	modalMininCart.addEventListener("click", function (e) {
		modalMininCart.classList.remove("minicart__dropdown_active");
		bodyDefault();
	});
	modalMininCartContent.addEventListener("click", function (e) {
		e.stopPropagation();
	});
	// END minicart

	function boduHidden() {
		body.classList.add("modal-open");
	}
	function bodyDefault() {
		body.classList.remove("modal-open");
	}






	// START btn-to-top
	var goTopBtn = document.querySelector('.btn-to-top');
	window.addEventListener('scroll', trackScroll);
	goTopBtn.addEventListener('click', backToTop);

	function trackScroll() {
		var scrolled = window.pageYOffset;
		var coords = document.documentElement.clientHeight;

		if (scrolled > coords) {
			goTopBtn.classList.add('btn-to-top_show');
		}
		if (scrolled < coords) {
			goTopBtn.classList.remove('btn-to-top_show');
		}
	}

	function backToTop() {
		if (window.pageYOffset > 0) {
			window.scrollBy(0, -80);
			setTimeout(backToTop, 0);
		}
	}
	// END btn-to-top
	

	// start slider price
	var sliderrange = jQuery('#slider-range');
	var toPrice = $( "#toPrice" );
	var fromPrice = $( "#fromPrice" );
	var maxPriceRange = $("#fromPrice").attr("max");
	// var amountprice = jQuery('#amount');
	sliderrange.slider({
		range: true,
		min: 0,
		max: maxPriceRange,
		values: [ 0, $("#fromPrice").val() ],
		slide: function(event, ui) {
			var $Handle = sliderrange.find('.ui-slider-handle');
			$Handle.css('margin-left', -1 * $Handle.width() * (sliderrange.slider('value') / sliderrange.slider('option', 'max')));

			toPrice.val( numberWithSpaces( ui.values[ 0 ] ) );
			fromPrice.val( numberWithSpaces( ui.values[ 1 ]) );
			
			//console.log(sliderrange.slider("values", 0));
			// amountprice.val("$" + sliderrange.slider("values", 0) + " - $" + sliderrange.slider("values", 1));
		}
	});
	var $Handle = sliderrange.find('.ui-slider-handle');
	$Handle.css('margin-left', -1 * $Handle.width() * (sliderrange.slider('value') / sliderrange.slider('option', 'max')));

	toPrice.val( numberWithSpaces( sliderrange.slider("values", 0) ) );
	fromPrice.val( numberWithSpaces( sliderrange.slider("values", 1) ) );

	toPrice.keypress(function(event) {
		if ( event.charCode && event.charCode!=0 && event.charCode!=46 && (event.charCode < 48 || event.charCode > 57 ) )
			return false;

		onlyNumberFromInput( this );
		// Изменить цену в слайдере
		sliderRangeChenge( 0, $(this).val() );
	});
	toPrice.on("input", function (event) {
		if ( event.charCode && event.charCode!=0 && event.charCode!=46 && (event.charCode < 48 || event.charCode > 57 ) )
			return false;

		onlyNumberFromInput( this );
		// Изменить цену в слайдере
		sliderRangeChenge( 0, $(this).val() );
	});

	fromPrice.keypress(function(event) {
		if ( event.charCode && event.charCode!=0 && event.charCode!=46 && (event.charCode < 48 || event.charCode > 57 ) )
			return false;

		onlyNumberFromInput( this );
		// Изменить цену в слайдере
		sliderRangeChenge( 1, $(this).val() );
	});
	fromPrice.on("input", function (event) {
		if ( event.charCode && event.charCode!=0 && event.charCode!=46 && (event.charCode < 48 || event.charCode > 57 ) )
			return false;

		onlyNumberFromInput( this);
		// Изменить цену в слайдере
		sliderRangeChenge( 1, $(this).val() );
	});
	
	function inputNumberSpaceDischarge(that) {

		var result = $(that).val().replace(/\s/g, '');
		result = parseInt(result);
		if ( isNaN(result) )
			return false;

		if(result) {
			
			if ( result > maxPriceRange ) {
				$(that).val( numberWithSpaces(maxPriceRange) );	
			}
			$(that).val( numberWithSpaces(result) );
		}		
	}
	function sliderRangeChenge(from, value) {
		value = parseInt( value.replace(/\s/g, '') );
		console.log(value);
		sliderrange.slider("values", from, value);
	}
	function onlyNumberFromInput( that ) {
		inputNumberSpaceDischarge(that);
	}
	function numberWithSpaces(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
	}
	// end slider price


	/*----------------------------
	Cart Plus Minus Button
	------------------------------ */
	// var CartPlusMinus = jQuery('.cart-plus-minus');
    // CartPlusMinus.prepend('<div class="dec qtybutton">-</div>');
    // CartPlusMinus.append('<div class="inc qtybutton">+</div>');
    // jQuery(".qtybutton").on("click", function () {
    //     var $button = jQuery(this);
    //     var target = $button.parent().find("input")
    //     var oldValue = target.val();
    //     if ($button.hasClass('inc')) {
    //         var newVal = parseFloat(oldValue) + 1;
    //     } else if ($button.hasClass('dec')) {
    //         if (oldValue > 0) {
    //             var newVal = parseFloat(oldValue) - 1;
    //         } else {
    //             newVal = 0;
    //         }
    //     }

    //     target.val(newVal).change();
	// });
	
	/*
		- picker-number кол-во элемента с кнопками + и -
		- проверка если nextVal == 0, то result(input.value) = 0
		- создание и вызов события change ( для ModX компонента)
	*/
	var numberPickers = document.querySelectorAll(".picker-number");

	numberPickers.forEach(function(item, index) {
		var buttons = item.querySelectorAll(".picker-number__button");

		// Событие на изменение значений
		// с помощью кнопок
		buttons.forEach(function(el) {
			el.addEventListener("click", function () {
				var target = this.parentNode.querySelector(".picker-number__input");
				var oldValue = target.value;
				var newValue;
				var maxValue = parseInt( target.getAttribute("max") );

				if ( this.classList.contains("picker-number__button_inc") ) {
					
					if ( maxValue ) {
						if (oldValue < maxValue) {
							newValue = parseInt( oldValue ) + 1;
						} else {
							newValue = maxValue;
						}
					} else {
						newValue = parseInt( oldValue ) + 1;
					}

				} 
				else if ( this.classList.contains("picker-number__button_dec") ) {		

					if (oldValue > 0) {
						newValue = parseInt( oldValue ) - 1;
					} else {
						newValue = 0;
					}

				}

				target.value = newValue;

				var event = new Event('change');
				target.dispatchEvent(event);
			});

		});

	});

	// Событие на изменение значений
	// ввода в input(с помощью клавиатуры)
	numberPickers.forEach(function(item){
		
		item.querySelector(".picker-number__input").addEventListener("input", function (e) {

			var minValue = parseInt( this.getAttribute("min") );
			var maxValue = parseInt( this.getAttribute("max") );

			if (maxValue) {

				if ( this.value >= maxValue) {
					this.value = maxValue;
				} else if ( this.value <= minValue) {
					this.value = minValue;
				}

			}

		});

	});












	// var navViewBlock = $(".type-view-nav");
	// var navViewButtons = $(navViewBlock).find(".type-view-nav_btn");
	// var navViewContent= $(".type-view-content");
	// $(navViewButtons).each(function(index, item) {
	// 	$(item).on("click", function () {
	// 		console.log(this);
	// 		if( $(item).attr("data-view") == 'list') {
	// 			$(navViewButtons).removeClass("active");
	// 			$(this).addClass("active");
	// 			$(navViewContent).addClass("list");
	// 		} else if ( $(item).attr("data-view") == 'grid' ) {
	// 			$(navViewButtons).removeClass("active");
	// 			$(this).addClass("active");
	// 			$(navViewContent).removeClass("list");
	// 		}
	// 	});
	// });



});
