$(document).ready(function () {
	$('.hero-01-slider').slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        prevArrow: false,
        nextArrow: false,
        autoplay: true,
        autoplaySpeed: 7000,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                  dots: false,
                }
            }
        ],
        customPaging: function (slider, i) {
            var title = $(slider.$slides[i].innerHTML).find('div[data-title]').data('title');
            return '<a class="pager__item"> ' + title + ' </a>';
        }
    });

    /*============================================
 
                    Product Slide
      
    =============================================*/

    $('.product-details-images').each(function () {
        var $this = $(this);
        var $thumb = $this.siblings('.product-details-thumbs');
        $this.slick({
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            infinite: true,
            centerMode: false,
            centerPadding: 0,
            asNavFor: $thumb,
        });
    });
    $('.product-details-thumbs').each(function () {
        var $this = $(this);
        var $details = $this.siblings('.product-details-images');
        $this.slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            infinite: true,
            focusOnSelect: true,
            centerMode: true,
            centerPadding: 0,
            prevArrow: '<button type="button"  class="slick-prev"><span class="icon-arrow-left"></span></button>',
            nextArrow: '<button type="button"  class="slick-next"><span class="icon-arrow-right"></span></button>',
            asNavFor: $details,
            responsive: [{
                    breakpoint: 1024,
                    settings: {}
                },
                {
                    breakpoint: 600,
                    settings: {}
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                    }
                }
            ]

        });
    });

    $('.slider-view').slick({
      dots: false,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: false,
      prevArrow: '<button type="button"  class="slick-prev"><span class="icon-arrow-left"></span></button>',
      nextArrow: '<button type="button"  class="slick-next"><span class="icon-arrow-right"></span></button>',
      responsive: [
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
      ]
  });

    $('.slider-hot-product').slick({
        dots: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: false,
        prevArrow: '<button type="button"  class="slick-prev"><span class="icon-arrow-left"></span></button>',
        nextArrow: '<button type="button"  class="slick-next"><span class="icon-arrow-right"></span></button>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 600,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              },
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
        ]
    });


    $(".slider-feedback").slick({
      dots: false,
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      autoplay: false,
      prevArrow: '<button type="button"  class="slick-prev"><span class="icon-arrow-left"></span></button>',
      nextArrow: '<button type="button"  class="slick-next"><span class="icon-arrow-right"></span></button>',
      responsive: [
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
      ]
  });

  $(".slider-ser").slick({
    dots: false,
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: false,
    prevArrow: '<button type="button"  class="slick-prev"><span class="icon-arrow-left"></span></button>',
    nextArrow: '<button type="button"  class="slick-next"><span class="icon-arrow-right"></span></button>',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });



});
